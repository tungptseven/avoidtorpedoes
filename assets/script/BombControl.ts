// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import GameLayout from "./GameLayout"
import SubmarineControl from "./SubmarineControl"
import GameScene from "./GameScene"
import { SoundType } from "./AudioSourceControl"

const { ccclass, property } = cc._decorator

@ccclass
export default class BombControl extends cc.Component {
    gameControl: GameScene = null
    gameLayout: GameLayout = null
    submarineControl: SubmarineControl = null
    speed: number = 0

    // LIFE-CYCLE CALLBACKS:
    onCollisionEnter(other, self) {
        if (other.tag == 1) {
            this.gameLayout.stars--
            this.gameLayout.starCd = 15
            this.submarineControl.onFired()
            this.gameControl.audioSourceControl.playSound(SoundType.E_Sound_Die)
            this.node.destroy()
        }
    }

    onLoad() {
        this.gameControl = cc.Canvas.instance.node.getComponent('GameScene')
        this.gameLayout = cc.Canvas.instance.node.getChildByName('GameLayout').getComponent('GameLayout')
        this.submarineControl = cc.Canvas.instance.node.getChildByName('GameLayout').getChildByName('Submarine').getComponent('SubmarineControl')
    }

    start() {

    }

    update(dt: number) {
        this.speed -= 0.15
        this.node.y += this.speed

        if (this.node.y <= -290) {
            this.speed = 0
            this.node.destroy()
            this.gameLayout.gameScore++
        }
    }
}
