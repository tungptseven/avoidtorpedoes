// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import GameLayout from "./GameLayout"
import GameScene from "./GameScene"
import { SoundType } from "./AudioSourceControl"

const { ccclass, property } = cc._decorator

@ccclass
export default class StarControl extends cc.Component {
    gameControl: GameScene = null
    gameLayout: GameLayout = null

    // LIFE-CYCLE CALLBACKS:
    onCollisionEnter(other, self) {
        if (other.tag == 1) {
            this.gameLayout.stars++
            this.gameControl.audioSourceControl.playSound(SoundType.E_Sound_Star)
            this.node.destroy()
        }
    }

    onLoad() {
        this.gameControl = cc.Canvas.instance.node.getComponent('GameScene')
        this.gameLayout = cc.Canvas.instance.node.getChildByName('GameLayout').getComponent('GameLayout')
    }

    start() {
        setTimeout(() => {
            if (this.node) {
                this.node.destroy()
                this.gameLayout.starCd = 15
            }
        }, 5000)
    }

    // update (dt) {}
}
