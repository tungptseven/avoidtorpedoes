// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator

@ccclass
export default class ShipControl extends cc.Component {

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        if (this.node.x == 0) {
            this.node.runAction(cc.repeatForever(
                cc.sequence(
                    cc.moveTo(2, this.node.x - 4, this.node.y),
                    cc.moveTo(2, this.node.x + 2, this.node.y)
                )
            ))
        } else {
            this.node.runAction(cc.repeatForever(
                cc.sequence(
                    cc.moveTo(2, this.node.x + 4, this.node.y),
                    cc.moveTo(2, this.node.x - 2, this.node.y)
                )
            ))
        }
    }

    start() {

    }

    // update (dt) {}
}
