// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator

@ccclass
export default class SubmarineControl extends cc.Component {
    accLeft: boolean
    accRight: boolean
    accel: number = 200
    maxMoveSpeed: number = 350
    xSpeed: number
    leftScreen
    rightScreen

    @property(cc.Animation)
    anim: cc.Animation = null

    onKeyDown(event) {
        switch (event.keyCode) {
            case cc.macro.KEY.left:
                this.accLeft = true
                break
            case cc.macro.KEY.right:
                this.accRight = true
                break
        }
    }

    onKeyUp(event) {
        switch (event.keyCode) {
            case cc.macro.KEY.left:
                this.accLeft = false
                break
            case cc.macro.KEY.right:
                this.accRight = false
                break
        }
    }

    onFired() {
        this.anim.play(this.anim.getClips()[0].name)
    }
    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        this.accLeft = false
        this.accRight = false
        this.xSpeed = 0

        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this)
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this)

        this.leftScreen = this.node.parent.getChildByName('LeftScreen')
        this.rightScreen = this.node.parent.getChildByName('RightScreen')

        this.leftScreen.on(cc.Node.EventType.TOUCH_START, function (event) {
            this.accLeft = true
        }, this)

        this.leftScreen.on(cc.Node.EventType.TOUCH_END, function (event) {
            // event when leave the finger touch
            this.accLeft = false
        }, this)

        this.rightScreen.on(cc.Node.EventType.TOUCH_START, function (event) {
            // click event
            this.accRight = true
        }, this)

        this.rightScreen.on(cc.Node.EventType.TOUCH_END, function (event) {
            // event when leave the finger touch
            this.accRight = false
        }, this)
    }

    onDestroy() {
        cc.systemEvent.off(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this)
        cc.systemEvent.off(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this)
    }

    start() {

    }

    update(dt) {
        if (this.accLeft && this.node.x >= -390) {
            this.node.scaleX = -1
            this.node.x -= 5

        } else if (this.accRight && this.node.x <= 390) {
            this.node.x += 5
            this.node.scaleX = 1
        }
    }

}
