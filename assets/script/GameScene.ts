// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import GameLayout from "./GameLayout"
import HomeLayout from "./HomeLayout"
import AudioSourceControl from "./AudioSourceControl"

const { ccclass, property } = cc._decorator

@ccclass
export default class GameScene extends cc.Component {
    isMuted: boolean = false

    @property(GameLayout)
    gameLayout: GameLayout = null

    @property(HomeLayout)
    homeLayout: HomeLayout = null

    @property(cc.Node)
    nHowTo: cc.Node = null

    @property(cc.Node)
    nGameOver: cc.Node = null

    @property(AudioSourceControl)
    audioSourceControl: AudioSourceControl = null

    // LIFE-CYCLE CALLBACKS:

    onPlay() {
        this.gameLayout.node.active = true
        this.homeLayout.node.active = false
        this.nHowTo.active = false
        this.nGameOver.active = false
        this.gameLayout.onPlay(false)
        this.audioSourceControl.onStart(this.isMuted)
    }

    onHome() {
        this.gameLayout.node.active = false
        this.homeLayout.node.active = true
        this.nHowTo.active = false
        this.nGameOver.active = false
        this.audioSourceControl.onStart(this.isMuted)
    }

    onReplay() {
        this.gameLayout.node.active = true
        this.homeLayout.node.active = false
        this.nHowTo.active = false
        this.nGameOver.active = false
        this.gameLayout.onPlay()
        this.audioSourceControl.onStart(this.isMuted)
    }

    onHowToOpen() {
        this.nHowTo.active = true
    }

    onHowToClose() {
        this.gameLayout.node.active = false
        this.homeLayout.node.active = true
        this.nHowTo.active = false
        this.nGameOver.active = false
    }

    onSoundToggle() {
        this.isMuted = !this.isMuted
        if (this.isMuted) {
            this.audioSourceControl.pause()
            this.homeLayout.node.getChildByName('soundOffBtn').active = false
            this.homeLayout.node.getChildByName('soundOnBtn').active = true
        } else {
            this.audioSourceControl.resume()
            this.homeLayout.node.getChildByName('soundOffBtn').active = true
            this.homeLayout.node.getChildByName('soundOnBtn').active = false
        }
    }

    onGameOver() { }

    onLoad() {
        this.gameLayout.node.active = false
        this.homeLayout.node.active = true
        this.audioSourceControl.onStart(this.isMuted)
    }

    start() { }

    // update (dt) {}
}
