// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import SubmarineControl from "./SubmarineControl"

const { ccclass, property } = cc._decorator

export enum GameStatus {
    Game_Ready = 0,
    Game_Playing,
    Game_Over
}

@ccclass
export default class GameLayout extends cc.Component {
    gameStatus: GameStatus = GameStatus.Game_Ready
    gameScore: number = 0
    gameOverLayout = null
    homeLayout = null
    submarineControl: SubmarineControl = null
    bombCd: number = 1
    starCd: number = 10
    stars: number = 1

    @property(cc.Prefab)
    bombPrefab: cc.Prefab = null

    @property(cc.Prefab)
    starPrefab: cc.Prefab = null

    @property(cc.Label)
    scoreLbl: cc.Label = null

    @property(cc.Label)
    starLbl: cc.Label = null

    // LIFE-CYCLE CALLBACKS:
    gameOver() {
        this.node.stopAllActions()
        this.gameStatus = GameStatus.Game_Over
        this.node.active = false
        this.gameOverLayout.active = true
        this.gameOverLayout.getComponent('GameOverLayout').gameScore = this.gameScore
        this.node.getChildByName('Bombs').removeAllChildren()
        this.node.getChildByName('Star').removeAllChildren()
        this.node.getChildByName('Submarine').setPosition(cc.v2(0, -280))
    }

    onPlay(isReplay: boolean = true) {
        this.gameStatus = GameStatus.Game_Playing
        this.gameScore = 0
        this.stars = 1
        this.onSpawnBomb()
    }

    onBack() {
        this.node.stopAllActions()
        this.gameStatus = GameStatus.Game_Over
        this.node.active = false
        this.homeLayout.active = true
        this.node.getChildByName('Bombs').removeAllChildren()
        this.node.getChildByName('Star').removeAllChildren()
        this.node.getChildByName('Submarine').setPosition(cc.v2(0, -280))
    }

    onLoad() {
        var collisionManager = cc.director.getCollisionManager()
        collisionManager.enabled = true

        this.gameScore = 0
        this.stars = 1
        this.scoreLbl.string = this.gameScore.toString()
        this.gameStatus = GameStatus.Game_Playing
        this.gameOverLayout = cc.Canvas.instance.node.getChildByName('GameOverLayout')
        this.homeLayout = cc.Canvas.instance.node.getChildByName('HomeLayout')
        this.submarineControl = this.node.getChildByName('Submarine').getComponent('SubmarineControl')
    }

    onSpawnBomb() {
        let bomb = cc.instantiate(this.bombPrefab)
        this.node.getChildByName('Bombs').addChild(bomb)
        let minX = -400
        let maxX = 400
        bomb.y = 175
        bomb.x = minX + Math.random() * (maxX - minX)
    }

    onSpawnStar() {
        if (this.node.getChildByName('Star').childrenCount < 1) {
            let star = cc.instantiate(this.starPrefab)
            this.node.getChildByName('Star').addChild(star)
            let minX = -400
            let maxX = 400
            star.y = -270
            star.x = minX + Math.random() * (maxX - minX)
        }
    }

    start() {

    }

    update(dt) {
        if (this.gameStatus === GameStatus.Game_Playing) {
            this.bombCd -= dt
            this.starCd -= dt
            if (this.bombCd <= 0) {
                this.onSpawnBomb()
                this.bombCd = Math.random() * (0.3 - 0.15) + 0.15
            }
            if (this.starCd <= 0 && this.stars < 5) {
                this.onSpawnStar()
                this.starCd = 15
            }

            this.starLbl.string = this.stars.toString()
            this.scoreLbl.string = this.gameScore.toString()
        }

        if (this.stars == 0) {
            setTimeout(() => this.gameOver(), 200)
        }
    }
}
